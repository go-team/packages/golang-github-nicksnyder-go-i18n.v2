golang-github-nicksnyder-go-i18n.v2 (2.1.2-1) unstable; urgency=medium

  * New upstream version 2.1.2
  * Fix filenamemangle error in debian/watch
  * Change Section from devel to golang
  * Bump Standards-Version to 4.6.0 (no change)
  * Mark library package with "Multi-Arch: foreign"
  * Update test to new error message in Go 1.16 and newer.
    This fixes test error without breaking the test on Go 1.15 and older.
    Thanks to William Wilson (Canonical) for the patch!
    (Closes: #990236, #997571)

 -- Anthony Fok <foka@debian.org>  Mon, 29 Nov 2021 05:54:19 -0700

golang-github-nicksnyder-go-i18n.v2 (2.1.1-2) unstable; urgency=medium

  * Run "cme fix dpkg" and "wrap-and-sort -ast" which tidied up
    debian/control, debian/docs and debian/examples
  * Breaks and Replaces golang-github-nicksnyder-go-i18n-dev (>= 2) to
    clean up my mistake of uploading golang-github-nicksnyder-go-i18n/2.1.1
    which is pending removal (#974032).
    Thanks to Andreas Beckmann for the solution. (Closes: #973636)

 -- Anthony Fok <foka@debian.org>  Thu, 12 Nov 2020 13:19:49 -0700

golang-github-nicksnyder-go-i18n.v2 (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1
  * debian/gbp.conf: Add "dist = DEP14"
  * Bump debhelper dependency to "Build-Depends: debhelper-compat (= 13)"
  * Bump Standards-Version to 4.5.0 (no change)
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * debian/control: Update dependencies as per v2/go.mod
  * Write man page for goi18n(1), to be included in a future upload
  * debian/copyright: Update Unicode License and copyright years
    and remove unneeded Files-Excluded field
  * debian/changelog: Append entries from golang-github-nicksnyder-go-i18n
    from 1.5.0-1 to 1.10.0-2, and fix the ITP bug number in the
    golang-github-nicksnyder-go-i18n.v2 (2.0.2-1) Initial Release entry
  * Add myself to the list of Uploaders and to debian/copyright

 -- Anthony Fok <foka@debian.org>  Thu, 29 Oct 2020 18:38:31 -0600

golang-github-nicksnyder-go-i18n.v2 (2.0.3-1) unstable; urgency=medium

  [ Dawid Dziurla ]
  * New upstream version 2.0.3

  [ Stephen Gelman ]
  * Team upload
  * Rename golang-x-text-dev to golang-golang-x-text-dev

 -- Stephen Gelman <ssgelm@debian.org>  Sun, 02 Aug 2020 19:58:17 -0500

golang-github-nicksnyder-go-i18n.v2 (2.0.2-2) unstable; urgency=medium

  * d/copyright: mention unicode
  * d/control: bump standards

 -- Dawid Dziurla <dawidd0811@gmail.com>  Thu, 29 Aug 2019 11:07:14 +0200

golang-github-nicksnyder-go-i18n.v2 (2.0.2-1) unstable; urgency=medium

  * Initial release of golang-github-nicksnyder-go-i18n.v2 (Closes: #931221)
    (The original changelog entry inadvertently closed #929166)

 -- Dawid Dziurla <dawidd0811@gmail.com>  Fri, 28 Jun 2019 15:20:06 +0200

golang-github-nicksnyder-go-i18n (1.10.0-2) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * Update and fix debian/watch
  * Apply "cme fix dpkg" fixes to debian/control
  * Bump Standards-Version to 4.1.5 (no change)
  * Use debhelper (>= 11)
  * Fix autopkgtest error by installing more files.  Sample error:
        panic: open ../goi18n/testdata/expected/en-us.all.json:
               no such file or directory

 -- Anthony Fok <foka@debian.org>  Wed, 11 Jul 2018 01:04:53 -0600

golang-github-nicksnyder-go-i18n (1.10.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.10.0
    - Update d/copyright
  * Update to Standards-Version 4.1.1
    - Use Priority: optional
  * Remove hack in d/rules to build on mips/mipsel.
    The bug will probably be fixed on the buildds in a couple of
    weeks, and it's fine to ask for a rebuild of the package.
    See https://lists.debian.org/debian-wb-team/2017/10/msg00005.html
  * Use wrap-and-sort

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 23 Nov 2017 14:37:12 +0100

golang-github-nicksnyder-go-i18n (1.8.1-2) unstable; urgency=medium

  * Try to beat the "mips-linux-gnu-gccgo-7: waitid: bad address" error
    on mips/mipsel until #867358 is resolved in the Linux kernel.
    See https://bugs.debian.org/867358

 -- Anthony Fok <foka@debian.org>  Sun, 30 Jul 2017 09:08:01 -0600

golang-github-nicksnyder-go-i18n (1.8.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.8.1
  * Add golang-github-pelletier-go-toml-dev as new dependency
  * Use HTTPS URL for d/copyright
  * Update to Standards-Version 4.0.0
  * Add Testsuite: autopkgtest-pkg-go to d/control

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 05 Jul 2017 21:36:46 +0200

golang-github-nicksnyder-go-i18n (1.7.0-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 1.7.0
  * Use debhelper v10

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 19 Dec 2016 23:35:32 +0100

golang-github-nicksnyder-go-i18n (1.5.0-1) unstable; urgency=medium

  * Initial release (Closes: #840803)
  * debian/copyright: Add entry for i18n/language/codegen/plurals.xml
    which is under its own license Unicode Data Files license rather than
    the MIT (Expat) license like the rest of github.com/nicksnyder/go-i18n.

 -- Anthony Fok <foka@debian.org>  Sun, 16 Oct 2016 01:30:11 -0600
